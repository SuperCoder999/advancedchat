## Installation

```bash
npm install # Will install dependencies in all three projects
```

In both client and server folders create .env (and .env.mongo) files and fill them using .env.example (and .env.mongo.example)

## Run

Client or server: `npm start`

MongoDB in Docker (run in server folder): `docker-compose up -d`

P.S. Possible to run without server. Just don't change client's API_BASE_URL which you copy from .env.example

## Demo Data (Users)

**User names:** Admin, Ben, Ruth, Wendy, Helen

**User email can be evaluated using:** `${username.toLowerCase()}@gmail.com`

**All users have `12345678` password except for Admin, who has `admin` password**
