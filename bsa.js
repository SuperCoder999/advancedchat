import Chat from "./client/src/containers/Chat";
import { combinedReducer as rootReducer } from "./client/src/redux/reducer";

export {
	Chat,
	rootReducer,
};
