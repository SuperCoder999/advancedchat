import React from "react";
import classnames from "../../helpers/classnames.helper";
import styles from "./button.module.scss";

interface Props {
	disabled?: boolean;
	className?: string;
	onClick?: () => void;
}

const Button: React.FC<Props> = ({
	children,
	disabled,
	className,
	onClick,
}) => {
	return (
		<div
			className={classnames(styles.button, className, {
				[styles.disabled]: disabled,
			})}
			onClick={() => onClick && !disabled && onClick()}
		>
			{children}
		</div>
	);
};

export default Button;
