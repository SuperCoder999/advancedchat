import React from "react";
import styles from "./checkbox.module.scss";

interface Props {
	placeholder?: string;
	value?: boolean;
	defaultValue?: boolean;
	onChange: (value: boolean) => void;
}

const Checkbox: React.FC<Props> = ({
	placeholder,
	value,
	defaultValue,
	onChange,
}) => {
	return (
		<div className={styles.container}>
			<input
				type="checkbox"
				checked={value}
				defaultChecked={defaultValue}
				onChange={(event) => onChange(event.target.checked)}
			/>
			{placeholder ? <div>{placeholder}</div> : null}
		</div>
	);
};

export default Checkbox;
