import React from "react";
import classnames from "../../helpers/classnames.helper";
import Icon from "../Icon";
import styles from "./dimmer.module.scss";

interface Props {
	shown?: boolean;
	className?: string;
	closeButtonClassName?: string;
	onClose?: () => void;
}

const Dimmer: React.FC<Props> = ({ children, shown, className, onClose }) => {
	return (
		<div
			className={classnames(styles.dimmer, className, {
				[styles.hidden]: !shown,
			})}
		>
			<div className={styles.content}>
				{onClose ? (
					<div className={styles.closeContainer}>
						<Icon onClick={onClose}>close</Icon>
					</div>
				) : null}
				{children}
			</div>
		</div>
	);
};

export default Dimmer;
