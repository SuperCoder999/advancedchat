import React from "react";
import dayjs from "dayjs";
import classnames from "../../helpers/classnames.helper";
import Message from "../../typing/dto/message/message.dto";
import { IconName } from "../Icon/icons";
import Icon from "../Icon";
import styles from "./general-message.module.scss";

export interface MessageAction {
	icon: IconName;
	name: string;
	info?: string | number;
	iconClassName?: string;
	function?: (message: Message) => void;
}

interface Props {
	showAvatar?: boolean;
	actions?: MessageAction[];
	className?: string;
	children: Message;
}

const GeneralMessage: React.FC<Props> = ({
	children: message,
	showAvatar,
	actions,
	className,
}) => {
	return (
		<div className={classnames(styles.generalMessage, className)}>
			{message.user.avatar && showAvatar ? (
				<img
					alt="Avatar"
					className={classnames(styles.avatar, "message-user-avatar")}
					src={message.user.avatar}
				/>
			) : null}
			<div className={styles.texts}>
				<div className={styles.metatext}>
					<span className="message-user-name">
						{message.user.name}
					</span>
					<span className={styles.bullet}>&bull;</span>
					<span className="message-time">
						{dayjs(message.createdAt).format("HH:mm")}
					</span>
					{message.editedAt ? (
						<>
							<span className={styles.bullet}>&bull;</span>
							edited at {dayjs(message.editedAt).format("HH:mm")}
						</>
					) : null}
				</div>
				<span className="message-text">{message.text}</span>
				<div className={styles.actions}>
					{(actions ?? []).map((action, index) => (
						<div key={index}>
							<Icon
								hint={action.name}
								className={action.iconClassName}
								onClick={
									action.function
										? () => action.function!(message)
										: undefined
								}
							>
								{action.icon}
							</Icon>
							{action.info !== undefined ? (
								<div className={styles.info}>{action.info}</div>
							) : null}
						</div>
					))}
				</div>
			</div>
		</div>
	);
};

export default GeneralMessage;
