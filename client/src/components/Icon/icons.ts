import edit from "../../assets/icons/edit.svg";
import _delete from "../../assets/icons/delete.svg";
import like from "../../assets/icons/like.svg";
import eye from "../../assets/icons/eye.svg";
import eyeSlash from "../../assets/icons/eye-slash.svg";
import at from "../../assets/icons/at.svg";
import user from "../../assets/icons/user.svg";
import close from "../../assets/icons/close.svg";

const icons: Record<string, string> = {
	edit,
	delete: _delete,
	like,
	eye,
	"eye-slash": eyeSlash,
	at,
	user,
	close,
};

export type IconName = keyof typeof icons;

export default icons;
