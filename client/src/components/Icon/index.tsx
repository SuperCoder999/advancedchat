import React from "react";
import classnames from "../../helpers/classnames.helper";
import icons, { IconName } from "./icons";
import styles from "./icon.module.scss";

interface Props {
	children: IconName;
	hint?: string;
	className?: string;
	onClick?: () => void;
}

const Icon: React.FC<Props> = ({
	children: name,
	hint,
	className,
	onClick,
}) => {
	return (
		<img
			alt={name}
			title={hint}
			className={classnames(styles.icon, className, {
				[styles.active]: onClick,
			})}
			src={icons[name]}
			onClick={onClick}
		/>
	);
};

export default Icon;
