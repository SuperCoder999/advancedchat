import React, { useRef, useState } from "react";
import InputType from "../../enums/input-type.enum";
import classnames from "../../helpers/classnames.helper";
import Icon from "../Icon";
import { IconName } from "../Icon/icons";
import styles from "./input.module.scss";

interface Props<V extends string | number = string | number> {
	type?: InputType;
	icon?: IconName;
	value?: string | number;
	defaultValue?: string | number;
	placeholder?: string;
	className?: string;
	validate?: (value: V) => string | undefined;
	onChange?: (value: V) => void;
	onIconClick?: () => void;
}

function Input<V extends string | number>({
	type,
	icon,
	value,
	defaultValue,
	placeholder,
	className,
	validate,
	onChange,
	onIconClick,
}: Props<V>): JSX.Element {
	const inputRef = useRef<HTMLInputElement | null>(null);

	const [error, setError] = useState<string | null>(null);
	const [focused, setFocused] = useState<boolean>(false);

	const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
		const newValue = event.target.value as V;

		if (onChange) {
			onChange(newValue);
		}

		if (validate) {
			setError(validate(newValue) ?? null);
		}
	};

	return (
		<div>
			<div
				className={classnames(styles.wrapper, {
					[styles.error]: error !== null,
					[styles.focused]: focused,
				})}
			>
				<div
					className={styles.clickableWrapper}
					onClick={() => inputRef.current?.focus()}
				>
					<input
						ref={inputRef}
						type={type}
						value={value}
						defaultValue={defaultValue}
						placeholder={placeholder}
						className={classnames(styles.input, className)}
						onFocus={() => setFocused(true)}
						onBlur={() => setFocused(false)}
						onChange={handleChange}
					/>
				</div>
				{icon ? <Icon onClick={onIconClick}>{icon}</Icon> : null}
			</div>
			{error ? <div className={styles.errorMessage}>{error}</div> : null}
		</div>
	);
}

export default Input;
