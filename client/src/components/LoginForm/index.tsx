import React, { useState } from "react";
import Button from "../Button";
import Input from "../Input";
import PasswordInput from "../PasswordInput";
import Segment from "../Segment";
import UserLogin from "../../typing/dto/user/user-login.dto";
import styles from "./login-form.module.scss";
import { validateEmail } from "../../helpers/validation.helper";

interface Props {
	onSubmit: (data: UserLogin) => void;
}

const LoginForm: React.FC<Props> = ({ onSubmit }) => {
	const [email, setEmail] = useState<string>("");
	const [password, setPassword] = useState<string>("");

	const valuesValid = !validateEmail(email);

	const handleSubmit = () => {
		if (!valuesValid) {
			return;
		}

		onSubmit({ email, password });
		setEmail("");
		setPassword("");
	};

	return (
		<Segment padded className={styles.segment}>
			<h1>Log in to Reactive Chat</h1>
			<Input<string>
				icon="at"
				value={email}
				placeholder="Email"
				onChange={setEmail}
				validate={validateEmail}
			/>
			<PasswordInput
				value={password}
				placeholder="Password"
				onChange={setPassword}
			/>
			<Button disabled={!valuesValid} onClick={handleSubmit}>
				Log in
			</Button>
		</Segment>
	);
};

export default LoginForm;
