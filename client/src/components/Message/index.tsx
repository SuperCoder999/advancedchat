import React from "react";
import classnames from "../../helpers/classnames.helper";
import MessageModel from "../../typing/dto/message/message.dto";
import GeneralMessage from "../GeneralMessage";
import styles from "./message.module.scss";

interface Props {
	children: MessageModel;
	likeMessage: () => void;
}

const Message: React.FC<Props> = ({ children: message, likeMessage }) => {
	return (
		<GeneralMessage
			showAvatar
			className={classnames(styles.message, "message")}
			actions={[
				{
					name: "Like",
					info: message.likeCount ?? 0,
					icon: "like",
					iconClassName: "message-like",
					function: likeMessage,
				},
			]}
		>
			{message}
		</GeneralMessage>
	);
};

export default Message;
