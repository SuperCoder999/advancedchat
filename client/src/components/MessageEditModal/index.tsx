import React, { useEffect, useState } from "react";
import classnames from "../../helpers/classnames.helper";
import Button from "../Button";
import Dimmer from "../Dimmer";
import Segment from "../Segment";
import styles from "./message-edit-modal.module.scss";

interface Props {
	defaultText: string;
	shown?: boolean;
	onClose: () => void;
	onSubmit: (text: string) => void;
}

const MessageEditModal: React.FC<Props> = ({
	defaultText,
	shown,
	onClose,
	onSubmit,
}) => {
	const [text, setText] = useState<string>(defaultText);
	const trimmedText = text.trim();

	useEffect(() => {
		setText(defaultText);
	}, [defaultText]);

	const handleSubmit = () => {
		if (trimmedText) {
			onSubmit(trimmedText);
			setText("");
		}
	};

	return (
		<Dimmer
			shown={shown}
			className={classnames("edit-message-modal", {
				"modal-shown": shown,
			})}
			onClose={onClose}
			closeButtonClassName="edit-message-close"
		>
			<Segment padded className={styles.segment}>
				<textarea
					className={classnames(
						styles.textField,
						"edit-message-input"
					)}
					onChange={(event) => setText(event.target.value)}
					placeholder="Enter your message..."
					value={text}
					autoFocus
				></textarea>
				<Button
					disabled={!trimmedText}
					className="edit-message-button"
					onClick={handleSubmit}
				>
					Save
				</Button>
			</Segment>
		</Dimmer>
	);
};

export default MessageEditModal;
