import React, { useState } from "react";
import classnames from "../../helpers/classnames.helper";
import styles from "./message-input.module.scss";

interface Props {
	onSubmit: (text: string) => void;
}

const MessageInput: React.FC<Props> = ({ onSubmit }) => {
	const [text, setText] = useState<string>("");
	const trimmedText = text.trim();

	const submit = () => {
		if (trimmedText) {
			onSubmit(trimmedText);
			setText("");
		}
	};

	return (
		<div className={classnames(styles.input, "message-input")}>
			<textarea
				className={classnames(styles.textField, "message-input-text")}
				onChange={(event) => setText(event.target.value)}
				placeholder="Enter your message..."
				value={text}
				autoFocus
			></textarea>
			<div
				className={classnames(
					styles.submitButton,
					"message-input-button",
					{ [styles.disabled]: !trimmedText }
				)}
				onClick={submit}
				title={trimmedText ? "Send" : "Enter text first :)"}
			>
				&#127876;
			</div>
		</div>
	);
};

export default MessageInput;
