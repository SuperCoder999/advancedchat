import React from "react";
import classnames from "../../helpers/classnames.helper";
import MessageModel from "../../typing/dto/message/message.dto";
import GeneralMessage from "../GeneralMessage";
import styles from "./own-message.module.scss";

interface Props {
	children: MessageModel;
	updateMessage: () => void;
	deleteMessage: () => void;
}

const OwnMessage: React.FC<Props> = ({
	children: message,
	updateMessage,
	deleteMessage,
}) => {
	return (
		<GeneralMessage
			className={classnames(styles.ownMessage, "own-message")}
			actions={[
				{
					name: "Edit",
					icon: "edit",
					iconClassName: "message-edit",
					function: updateMessage,
				},
				{
					name: "Delete",
					icon: "delete",
					iconClassName: "message-delete",
					function: deleteMessage,
				},
				{
					name: "Like",
					info: message.likeCount ?? 0,
					icon: "like",
					iconClassName: "message-like",
				},
			]}
		>
			{message}
		</GeneralMessage>
	);
};

export default OwnMessage;
