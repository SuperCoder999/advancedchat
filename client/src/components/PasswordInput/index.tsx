import React, { useEffect, useState } from "react";
import InputType from "../../enums/input-type.enum";
import Input from "../Input";

interface Props {
	value?: string;
	defaultValue?: string;
	placeholder?: string;
	className?: string;
	validate?: (value: string) => string | undefined;
	onChange?: (value: string) => void;
}

const PasswordInput: React.FC<Props> = ({
	value,
	defaultValue,
	placeholder,
	className,
	validate,
	onChange,
}) => {
	const [password, setPassword] = useState<string>(
		value ?? defaultValue ?? ""
	);

	useEffect(() => {
		setPassword(value ?? defaultValue ?? "");
	}, [value, defaultValue]);

	const [hidden, setHidden] = useState<boolean>(true);

	const handleChange = (value: string) => {
		if (onChange) {
			onChange(value);
		}

		setPassword(value);
	};

	return (
		<Input<string>
			type={hidden ? InputType.Password : InputType.Text}
			icon={hidden ? "eye" : "eye-slash"}
			value={password}
			placeholder={placeholder}
			className={className}
			validate={validate}
			onChange={handleChange}
			onIconClick={() => setHidden(!hidden)}
		/>
	);
};

export default PasswordInput;
