import React from "react";
import Dimmer from "../Dimmer";
import styles from "./preloader.module.scss";

interface Props {
	loading?: boolean;
}

const Preloader: React.FC<Props> = ({ loading }) => {
	return (
		<Dimmer shown={loading} className="preloader">
			<div className={styles.loader} />
		</Dimmer>
	);
};

export default Preloader;
