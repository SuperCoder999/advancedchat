import React from "react";
import { Navigate } from "react-router";
import User from "../../typing/dto/user/user.dto";

interface Props {
	user: User | null;
	onlyAdmin?: boolean;
	children: JSX.Element;
}

const PrivateRoute: React.FC<Props> = ({
	children = null,
	onlyAdmin,
	user,
}) => {
	if (!user) {
		return <Navigate to="/login" />;
	}

	if (onlyAdmin && !user.isAdmin) {
		return <Navigate to="/" />;
	}

	return children;
};

export default PrivateRoute;
