import React from "react";
import { Navigate } from "react-router";
import User from "../../typing/dto/user/user.dto";

interface Props {
	user: User | null;
	children: JSX.Element;
}

const PublicRoute: React.FC<Props> = ({ children = null, user }) => {
	return !user ? children : <Navigate to="/" />;
};

export default PublicRoute;
