import React from "react";
import classnames from "../../helpers/classnames.helper";
import styles from "./segment.module.scss";

interface Props {
	padded?: boolean;
	className?: string;
}

const Segment: React.FC<Props> = ({ children, padded, className }) => {
	return (
		<div
			className={classnames(styles.segment, className, {
				[styles.padded]: padded,
			})}
		>
			{children}
		</div>
	);
};

export default Segment;
