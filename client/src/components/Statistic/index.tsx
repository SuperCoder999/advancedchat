import React from "react";
import classnames from "../../helpers/classnames.helper";
import styles from "./statistic.module.scss";

interface Props {
	children: string | number;
	label: string;
	className?: string;
	valueClassName?: string;
	labelClassName?: string;
}

const Statistic: React.FC<Props> = ({
	children: value,
	label,
	className,
	valueClassName,
	labelClassName,
}) => {
	return (
		<div className={classnames(styles.statistic, className)}>
			<span className={classnames(styles.value, valueClassName)}>
				{value}
			</span>
			<span className={classnames(styles.label, labelClassName)}>
				{label}
			</span>
		</div>
	);
};

export default Statistic;
