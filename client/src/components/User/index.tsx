import React from "react";
import { useNavigate } from "react-router";
import UserModel from "../../typing/dto/user/user.dto";
import Icon from "../Icon";
import styles from "./user.module.scss";

interface Props {
	children: UserModel;
	deleteUser: (id: string) => void;
}

const User: React.FC<Props> = ({ children: user, deleteUser }) => {
	const navigate = useNavigate();

	return (
		<div className={styles.user}>
			{user.avatar ? (
				<img alt="Avatar" className={styles.avatar} src={user.avatar} />
			) : null}
			<div>{user.name}</div>
			<div>{user.email}</div>
			{user.isAdmin ? <div className={styles.isAdmin}>Admin</div> : null}
			<Icon onClick={() => navigate(`/users/edit/${user._id}`)}>
				edit
			</Icon>
			<Icon onClick={() => deleteUser(user._id)}>delete</Icon>
		</div>
	);
};

export default User;
