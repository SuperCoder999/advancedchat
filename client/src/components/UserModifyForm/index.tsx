import React, { useEffect, useState } from "react";

import {
	validateName,
	validateEmail,
	getValidatePassword,
	validateAvatarLink,
} from "../../helpers/validation.helper";

import UserModify from "../../typing/dto/user/user-modify.dto";
import User from "../../typing/dto/user/user.dto";
import Button from "../Button";
import Checkbox from "../Checkbox";
import Input from "../Input";
import PasswordInput from "../PasswordInput";
import Segment from "../Segment";
import styles from "./user-modify-form.module.scss";

interface Props {
	defaultUser?: User;
	onSubmit: (data: UserModify) => void;
}

const UserModifyForm: React.FC<Props> = ({ defaultUser, onSubmit }) => {
	const [name, setName] = useState<string>(defaultUser?.name ?? "");
	const [email, setEmail] = useState<string>(defaultUser?.email ?? "");
	const [avatar, setAvatar] = useState<string>(defaultUser?.avatar ?? "");
	const [password, setPassword] = useState<string>("");

	const [isAdmin, setIsAdmin] = useState<boolean>(
		defaultUser?.isAdmin ?? false
	);

	useEffect(() => {
		setName(defaultUser?.name ?? "");
		setEmail(defaultUser?.email ?? "");
		setAvatar(defaultUser?.avatar ?? "");
		setPassword("");
		setIsAdmin(defaultUser?.isAdmin ?? false);
	}, [defaultUser]);

	const validatePassword = getValidatePassword(Boolean(defaultUser));

	const valuesValid =
		!validateName(name) &&
		!validateEmail(email) &&
		!validatePassword(password);

	const handleSubmit = () => {
		if (!valuesValid) {
			return;
		}

		onSubmit({
			name,
			email,
			...(password ? { password } : {}),
			...(avatar ? { avatar } : {}),
			isAdmin,
		});

		setName("");
		setEmail("");
		setAvatar("");
		setPassword("");
		setIsAdmin(false);
	};

	return (
		<Segment padded className={styles.segment}>
			<h1>{defaultUser ? "Edit" : "Add"} user</h1>
			<Input
				icon="user"
				value={name}
				placeholder="Name"
				validate={validateName}
				onChange={setName}
			/>
			<Input
				icon="at"
				value={email}
				placeholder="Email"
				validate={validateEmail}
				onChange={setEmail}
			/>
			<Input
				icon="user"
				value={avatar}
				placeholder="Avatar link"
				validate={validateAvatarLink}
				onChange={setAvatar}
			/>
			<PasswordInput
				value={password}
				placeholder={
					defaultUser
						? "Enter something to change password..."
						: "Password"
				}
				validate={validatePassword}
				onChange={setPassword}
			/>
			<Checkbox
				value={isAdmin}
				placeholder="Is admin"
				onChange={setIsAdmin}
			/>
			<Button onClick={handleSubmit}>Save</Button>
		</Segment>
	);
};

export default UserModifyForm;
