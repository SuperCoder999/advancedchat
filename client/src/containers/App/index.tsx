import React from "react";
import { Provider as ReduxProvider } from "react-redux";
import { NotificationContainer } from "react-notifications";
import store from "../../redux/store";
import Routing from "../Routing";

const App: React.FC = () => {
	return (
		<ReduxProvider store={store}>
			<NotificationContainer />
			<Routing />
		</ReduxProvider>
	);
};

export default App;
