import React, { useCallback, useEffect } from "react";
import { useSelector } from "react-redux";
import MessageInput from "../../components/MessageInput";
import Preloader from "../../components/Preloader";
import { replaceGlobalEventListener } from "../../helpers/global-events.helper";
import { usePromiseDispatch } from "../../redux/store";
import RootState from "../../redux/state";

import {
	loadMessages,
	createMessage,
	updateMessage,
	likeMessage,
	deleteMessage,
	startUpdatingMessage,
	cancelUpdatingMessage,
} from "../../redux/chat/actions";

import Message from "../../typing/dto/message/message.dto";
import User from "../../typing/dto/user/user.dto";
import MessageList from "../MessageList";
import MessageEditModal from "../../components/MessageEditModal";
import classnames from "../../helpers/classnames.helper";
import styles from "./chat.module.scss";

const Chat: React.FC = () => {
	const dispatch = usePromiseDispatch();

	const {
		user,
		messages,
		messagesLoaded,
		showEditModal,
		updatingMessage,
		preloader,
	} = useSelector((state: RootState) => ({
		user: state.auth.user as User,
		messages: state.chat.messages,
		messagesLoaded: state.chat.messagesLoaded,
		showEditModal: state.chat.editModal,
		updatingMessage: state.chat.updatingMessage as Message,
		preloader: state.chat.preloader,
	}));

	useEffect(() => {
		if (!messagesLoaded) {
			dispatch(loadMessages());
		}
	}, [dispatch, messagesLoaded]);

	useEffect(() => {
		replaceGlobalEventListener(1, "keyup", (event: Event) => {
			const keyboardEvent = event as KeyboardEvent;

			if (keyboardEvent.code === "ArrowUp" && messages) {
				const reversedMessages = [...messages].reverse();

				const lastOwnMessage = reversedMessages.find(
					(message) => message.user._id === user._id
				); // Messages are sorted by date

				if (lastOwnMessage) {
					dispatch(startUpdatingMessage(lastOwnMessage));
				}
			}
		});
	}, [dispatch, messages, user._id]);

	const create = useCallback(
		(text: string) => dispatch(createMessage(text)),
		[dispatch]
	);

	const like = useCallback(
		(id: string) => dispatch(likeMessage(id)),
		[dispatch]
	);

	const update = useCallback(
		(id: string, text: string) => dispatch(updateMessage({ id, text })),
		[dispatch]
	);

	const remove = useCallback(
		(id: string) => dispatch(deleteMessage(id)),
		[dispatch]
	);

	return (
		<div className={classnames(styles.chat, "chat")}>
			<Preloader loading={preloader} />
			<MessageList
				likeMessage={(message) => like(message._id)}
				updateMessage={(message) =>
					dispatch(startUpdatingMessage(message))
				}
				deleteMessage={(message) => remove(message._id)}
			/>
			<MessageInput onSubmit={create} />
			<MessageEditModal
				shown={showEditModal}
				defaultText={updatingMessage?.text ?? ""}
				onClose={() => dispatch(cancelUpdatingMessage())}
				onSubmit={(text) =>
					updatingMessage && update(updatingMessage._id, text)
				}
			/>
		</div>
	);
};

export default Chat;
