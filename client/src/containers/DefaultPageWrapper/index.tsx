import React, { useEffect } from "react";
import { useSelector } from "react-redux";
import { useNavigate } from "react-router";
import Preloader from "../../components/Preloader";
import { loadMessagesInfo } from "../../redux/chat/actions";
import RootState from "../../redux/state";
import { usePromiseDispatch } from "../../redux/store";
import Header from "../Header";
import styles from "./default-page-wrapper.module.scss";

const DefaultPageWrapper: React.FC = ({ children }) => {
	const dispatch = usePromiseDispatch();
	const navigate = useNavigate();

	const info = useSelector((state: RootState) => state.chat.info);

	useEffect(() => {
		if (!info) {
			dispatch(loadMessagesInfo());
		}
	}, [dispatch, navigate, info]);

	if (!info) {
		return <Preloader loading />;
	}

	return (
		<div className={styles.container}>
			<Header />
			{children}
		</div>
	);
};

export default DefaultPageWrapper;
