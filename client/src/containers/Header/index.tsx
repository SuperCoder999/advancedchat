import React from "react";
import dayjs from "dayjs";
import { useSelector } from "react-redux";
import { useNavigate } from "react-router";
import RootState from "../../redux/state";
import { usePromiseDispatch } from "../../redux/store";
import { logout } from "../../redux/auth/actions";
import Statistic from "../../components/Statistic";
import classnames from "../../helpers/classnames.helper";
import MessagesInfo from "../../typing/dto/message/messages-info.dto";
import User from "../../typing/dto/user/user.dto";
import styles from "./header.module.scss";

const Header: React.FC = () => {
	const dispatch = usePromiseDispatch();
	const navigate = useNavigate();

	const {
		user: { isAdmin, name: username },
		info: { usersCount, messagesCount, lastMessageDate },
	} = useSelector((state: RootState) => ({
		user: state.auth.user as User,
		info: state.chat.info as MessagesInfo,
	}));

	const handleLogout = () => {
		dispatch(logout());
	};

	const handleUsersPage = () => {
		navigate("/users");
	};

	const handleChatPage = () => {
		navigate("/");
	};

	return (
		<div className={classnames(styles.header, "header")}>
			<span className={classnames(styles.title, "header-title")}>
				&#127876; Reactive Chat
			</span>
			<div className={styles.metrics}>
				<Statistic label="Users" valueClassName="header-users-count">
					{usersCount}
				</Statistic>
				<Statistic
					label="Messages"
					valueClassName="header-messages-count"
				>
					{messagesCount}
				</Statistic>
				{lastMessageDate ? (
					<span className={styles.lastMessageDate}>
						Last message{" "}
						<span className="header-last-message-date">
							{dayjs(lastMessageDate).format("DD.MM.YYYY HH:mm")}
						</span>
					</span>
				) : null}
				<div className={styles.username}>{username}</div>
				<div className={styles.button} onClick={handleLogout}>
					Log out
				</div>
				{isAdmin ? (
					<>
						<div
							className={styles.button}
							onClick={handleUsersPage}
						>
							Edit users
						</div>
						<div className={styles.button} onClick={handleChatPage}>
							Go to chat
						</div>
					</>
				) : null}
			</div>
		</div>
	);
};

export default Header;
