import React from "react";
import LoginForm from "../../components/LoginForm";
import { login } from "../../redux/auth/actions";
import { usePromiseDispatch } from "../../redux/store";
import UserLogin from "../../typing/dto/user/user-login.dto";
import styles from "./login.module.scss";

const Login: React.FC = () => {
	const dispatch = usePromiseDispatch();

	const onSubmit = (data: UserLogin) => {
		dispatch(login(data))
			.unwrap()
			.then(({ user }) =>
				window.location.assign(user?.isAdmin ? "/users" : "/")
			);
	};

	return (
		<div className={styles.background}>
			<LoginForm onSubmit={onSubmit} />
		</div>
	);
};

export default Login;
