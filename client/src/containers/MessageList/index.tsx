import dayjs from "dayjs";
import React, { useCallback } from "react";
import { useSelector } from "react-redux";
import Divider from "../../components/Divider";
import Message from "../../components/Message";
import OwnMessage from "../../components/OwnMessage";
import classnames from "../../helpers/classnames.helper";
import { getMessageDividerText } from "../../helpers/date.helper";
import RootState from "../../redux/state";
import MessageModel from "../../typing/dto/message/message.dto";
import User from "../../typing/dto/user/user.dto";
import styles from "./message-list.module.scss";

interface Props {
	likeMessage: (message: MessageModel) => void;
	updateMessage: (message: MessageModel) => void;
	deleteMessage: (message: MessageModel) => void;
}

const MessageList: React.FC<Props> = ({
	likeMessage,
	updateMessage,
	deleteMessage,
}) => {
	const {
		messages,
		user: { _id: currentUserId },
	} = useSelector((state: RootState) => ({
		messages: state.chat.messages as MessageModel[],
		user: state.auth.user as User,
	}));

	let lastMessageDividerDate: Date | null = null;

	const getMessageElement = useCallback(
		(message: MessageModel) => {
			return message.user._id === currentUserId ? (
				<OwnMessage
					key={message._id}
					updateMessage={() => updateMessage(message)}
					deleteMessage={() => deleteMessage(message)}
				>
					{message}
				</OwnMessage>
			) : (
				<Message
					key={message._id}
					likeMessage={() => likeMessage(message)}
				>
					{message}
				</Message>
			);
		},
		[currentUserId, likeMessage, updateMessage, deleteMessage]
	);

	return (
		<div className={classnames(styles.messageList, "message-list")}>
			{messages.map((message) => {
				const element = getMessageElement(message);

				const dateDiff: number | null = lastMessageDividerDate
					? dayjs(lastMessageDividerDate).diff(
							dayjs(message.createdAt),
							"days"
					  )
					: null;

				if (!lastMessageDividerDate || Math.abs(dateDiff ?? 0) >= 1) {
					lastMessageDividerDate = message.createdAt;

					return (
						<React.Fragment key={message._id}>
							<Divider className="messages-divider">
								{getMessageDividerText(lastMessageDividerDate)}
							</Divider>
							{element}
						</React.Fragment>
					);
				}

				return element;
			})}
		</div>
	);
};

export default MessageList;
