import { useEffect } from "react";
import { useSelector } from "react-redux";
import { Routes, Route } from "react-router";
import { BrowserRouter } from "react-router-dom";
import Preloader from "../../components/Preloader";
import PrivateRoute from "../../components/PrivateRoute";
import PublicRoute from "../../components/PublicRoute";
import forceRedirectLogin from "../../helpers/redirect.helper";
import ChatPage from "../../pages/ChatPage";
import LoginPage from "../../pages/LoginPage";
import UsersPage from "../../pages/UsersPage";
import { loadProfile } from "../../redux/auth/actions";
import RootState from "../../redux/state";
import { usePromiseDispatch } from "../../redux/store";

const Routing: React.FC = () => {
	const dispatch = usePromiseDispatch();

	const { user, preloader, userLoaded } = useSelector(
		(state: RootState) => state.auth
	);

	useEffect(() => {
		if (!userLoaded) {
			dispatch(loadProfile())
				.unwrap()
				.catch(() => forceRedirectLogin());
		}
	}, [dispatch, userLoaded]);

	if (!userLoaded) {
		return <Preloader loading />;
	}

	return (
		<>
			<Preloader loading={preloader} />
			<BrowserRouter>
				<Routes>
					<Route
						path="/login"
						element={
							<PublicRoute user={user}>
								<LoginPage />
							</PublicRoute>
						}
					/>
					<Route
						path="/users"
						element={
							<PrivateRoute user={user} onlyAdmin>
								<UsersPage />
							</PrivateRoute>
						}
					/>
					<Route
						path="/users/edit"
						element={
							<PrivateRoute user={user} onlyAdmin>
								<UsersPage modifying />
							</PrivateRoute>
						}
					/>
					<Route
						path="/users/edit/:editId"
						element={
							<PrivateRoute user={user} onlyAdmin>
								<UsersPage modifying />
							</PrivateRoute>
						}
					/>
					<Route
						path="/*"
						element={
							<PrivateRoute user={user}>
								<ChatPage />
							</PrivateRoute>
						}
					/>
				</Routes>
			</BrowserRouter>
		</>
	);
};

export default Routing;
