import React, { useCallback, useEffect, useMemo } from "react";
import { useSelector } from "react-redux";
import { useNavigate, useParams } from "react-router";
import Button from "../../components/Button";
import Dimmer from "../../components/Dimmer";
import Preloader from "../../components/Preloader";
import User from "../../components/User";
import UserModifyForm from "../../components/UserModifyForm";
import RootState from "../../redux/state";
import { usePromiseDispatch } from "../../redux/store";

import {
	createUser,
	deleteUser,
	loadUsers,
	updateUser,
} from "../../redux/users/actions";

import UserModify from "../../typing/dto/user/user-modify.dto";
import styles from "./user-list.module.scss";

interface Props {
	modifying?: boolean;
}

const UserList: React.FC<Props> = ({ modifying }) => {
	const { editId } = useParams();
	const navigate = useNavigate();

	const dispatch = usePromiseDispatch();

	const { users, usersLoaded, preloader } = useSelector(
		(state: RootState) => state.users
	);

	const updatingUser = useMemo(
		() => users.find((u) => u._id === editId),
		[editId, users]
	);

	useEffect(() => {
		if (!usersLoaded) {
			dispatch(loadUsers());
		}
	}, [dispatch, usersLoaded]);

	const create = useCallback(
		(data: UserModify) => dispatch(createUser(data)),
		[dispatch]
	);

	const update = useCallback(
		(id: string, data: UserModify) => dispatch(updateUser({ id, data })),
		[dispatch]
	);

	const remove = useCallback(
		(id: string) => dispatch(deleteUser(id)),
		[dispatch]
	);

	const handleFormSubmit = (data: UserModify) => {
		if (editId) {
			update(editId, data);
		} else {
			create(data);
		}

		navigate("/users");
	};

	return (
		<div className={styles.users}>
			<Preloader loading={preloader} />
			<Dimmer
				shown={Boolean(
					!preloader && modifying && (editId ? updatingUser : true)
				)}
				onClose={() => navigate("/users")}
			>
				<UserModifyForm
					defaultUser={updatingUser}
					onSubmit={handleFormSubmit}
				/>
			</Dimmer>
			<Button onClick={() => navigate("/users/edit")}>Add user</Button>
			{users.map((user) => (
				<User deleteUser={remove} key={user._id}>
					{user}
				</User>
			))}
		</div>
	);
};

export default UserList;
