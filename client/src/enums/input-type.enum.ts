enum InputType {
	Text = "text",
	Number = "number",
	Password = "password",
}

export default InputType;
