enum LocalStorageKeys {
	UserToken = "user:auth:token",
}

export default LocalStorageKeys;
