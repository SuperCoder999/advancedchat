import { NotificationManager } from "react-notifications";
import LocalStorageKeys from "../enums/local-storage-keys.enum";
import HttpError from "../exceptions/http-error";
import { RequestArgs } from "../typing/http";

const API_BASE_URL =
	process.env.REACT_APP_API_BASE_URL ??
	"https://reactive-chat-21-winter-server.herokuapp.com";

export default async function callWebApi(args: RequestArgs): Promise<Response> {
	try {
		const response: Response = await fetch(
			getRequestUrl(args),
			getRequestInit(args)
		);

		await throwIfRequestFailed(response);
		return response;
	} catch (error) {
		NotificationManager.error((error as Error).message, "Error");
		throw error;
	}
}

function getRequestUrl(args: RequestArgs): string {
	return `${API_BASE_URL}/api${args.endpoint}`;
}

function getRequestInit(args: RequestArgs): RequestInit {
	const token = localStorage.getItem(LocalStorageKeys.UserToken);

	const authorizationHeader = token
		? { Authorization: `Bearer ${token}` }
		: {};

	return {
		method: args.method,
		headers: {
			"Content-Type": args.body ? "application/json" : "text/plain",
			...authorizationHeader,
		} as HeadersInit,
		...(args.body ? { body: JSON.stringify(args.body) } : {}),
	};
}

async function throwIfRequestFailed(response: Response): Promise<void> {
	if (response.ok) {
		return;
	}

	let messageFromBody: string | undefined;

	try {
		const data = await response.json();
		messageFromBody = data.message;
	} catch {
		//
	}

	const errorMessage = messageFromBody ?? response.statusText;
	throw new HttpError(response.status, errorMessage);
}
