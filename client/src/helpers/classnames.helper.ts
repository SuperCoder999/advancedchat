type ClassnamesEnumerationItem = string | void | Record<string, any>;

export default function classnames(
	...enumeration: ClassnamesEnumerationItem[]
): string {
	const resultClassNames: string[] = [];

	enumeration.forEach((item) => {
		if (item !== null && typeof item === "object") {
			Object.entries(item).forEach(([className, append]) => {
				if (append) {
					resultClassNames.push(className);
				}
			});
		} else if (item) {
			resultClassNames.push(item);
		}
	});

	return resultClassNames.join(" ");
}
