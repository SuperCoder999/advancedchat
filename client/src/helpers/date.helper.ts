import dayjs from "dayjs";

export function getMessageDividerText(date: Date): string {
	const now = dayjs();
	const otherDate = new Date(date);

	otherDate.setMinutes(
		otherDate.getMinutes() - otherDate.getTimezoneOffset()
	);

	const other = dayjs(otherDate);
	const diff = now.diff(other, "days");

	if (diff === 0) {
		return "Today";
	} else if (diff === 1) {
		return "Yesterday";
	}

	return other.format("dddd, DD MMMM");
}
