type EventTypes = keyof HTMLElementEventMap;

const eventListeners: Partial<
	Record<EventTypes, Record<number, EventListener>>
> = {};

function addEventTypeToMap(event: EventTypes): void {
	if (!eventListeners[event]) {
		eventListeners[event] = {};
	}
}

export function addGlobalEventListener(
	id: number,
	event: EventTypes,
	listener: EventListener
): void {
	addEventTypeToMap(event);

	document.body.addEventListener(event, listener);
	eventListeners[event]![id] = listener;
}

export function replaceGlobalEventListener(
	id: number,
	event: EventTypes,
	listener: EventListener
): void {
	addEventTypeToMap(event);

	if (eventListeners[event]![id]) {
		document.body.removeEventListener(event, eventListeners[event]![id]);
	}

	document.body.addEventListener(event, listener);
	eventListeners[event]![id] = listener;
}

export function removeGlobalEventListener(id: number, event: EventTypes): void {
	addEventTypeToMap(event);

	if (!eventListeners[event]![id]) {
		return;
	}

	document.body.removeEventListener(event, eventListeners[event]![id]);
	delete eventListeners[event]![id];
}
