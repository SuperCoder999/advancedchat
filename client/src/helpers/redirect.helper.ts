import LocalStorageKeys from "../enums/local-storage-keys.enum";

export default function forceRedirectLogin() {
	localStorage.removeItem(LocalStorageKeys.UserToken);
	window.location.assign("/login");
}
