import validator from "validator";

export const validateName = (value: string): string | undefined => {
	return value.length === 0 ? "Name must not be empty" : undefined;
};

export const validateEmail = (value: string): string | undefined => {
	return validator.isEmail(value) ? undefined : "Invalid email";
};

export const getValidatePassword =
	(allowEmpty: boolean = false) =>
	(value: string): string | undefined => {
		const isLonger8 = value.length >= 8;

		const isValid = allowEmpty
			? isLonger8 || value.length === 0
			: isLonger8;

		return isValid
			? undefined
			: "Password must be at least 8 characters long";
	};

export const validateAvatarLink = (value: string): string | undefined => {
	return validator.isURL(value) || value.length === 0
		? undefined
		: "Invalid link";
};
