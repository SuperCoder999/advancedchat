import ReactDOM from "react-dom";
import App from "./containers/App";
import reportWebVitals from "./reportWebVitals";
import "react-notifications/lib/notifications.css";
import "./styles/index.scss";

const root = document.getElementById("root");
ReactDOM.render(<App />, root);

reportWebVitals();
