import React from "react";
import Chat from "../../containers/Chat";
import DefaultPageWrapper from "../../containers/DefaultPageWrapper";

const ChatPage: React.FC = () => {
	return (
		<DefaultPageWrapper>
			<Chat />
		</DefaultPageWrapper>
	);
};

export default ChatPage;
