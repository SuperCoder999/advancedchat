import React from "react";
import DefaultPageWrapper from "../../containers/DefaultPageWrapper";
import UserList from "../../containers/UserList";

interface Props {
	modifying?: boolean;
}

const UsersPage: React.FC<Props> = ({ modifying }) => {
	return (
		<DefaultPageWrapper>
			<UserList modifying={modifying} />
		</DefaultPageWrapper>
	);
};

export default UsersPage;
