enum AuthActions {
	Login = "auth:login",
	Logout = "auth:logout",
	LoadProfile = "auth:profile:load",
}

export default AuthActions;
