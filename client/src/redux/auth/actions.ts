import { createAsyncThunk } from "@reduxjs/toolkit";

import {
	authenticate,
	getProfile,
	logoutUser,
} from "../../services/auth.service";

import UserLogin from "../../typing/dto/user/user-login.dto";
import AuthActions from "./actionTypes";
import AuthState from "./state";

export const login = createAsyncThunk<Partial<AuthState>, UserLogin>(
	AuthActions.Login,
	async (payload: UserLogin) => ({
		user: await authenticate(payload),
	})
);

export const logout = createAsyncThunk<Partial<AuthState>>(
	AuthActions.Logout,
	async () => {
		logoutUser();
		return { user: null };
	}
);

export const loadProfile = createAsyncThunk<Partial<AuthState>>(
	AuthActions.LoadProfile,
	async () => ({
		user: await getProfile(),
	})
);
