import { createReducer, isAnyOf } from "@reduxjs/toolkit";
import { loadProfile, login, logout } from "./actions";
import AuthState, { initialState } from "./state";

const authReducer = createReducer<AuthState>(initialState, (builder) => {
	builder.addMatcher(
		isAnyOf(login.pending, logout.pending, loadProfile.pending),
		(state) => ({
			...state,
			preloader: true,
		})
	);

	builder.addMatcher(
		isAnyOf(login.fulfilled, logout.fulfilled, loadProfile.fulfilled),
		(state, { payload }) => ({
			...state,
			...payload,
			userLoaded: true,
			preloader: false,
		})
	);

	builder.addMatcher(
		isAnyOf(login.rejected, logout.rejected, loadProfile.rejected),
		(state) => ({
			...state,
			userLoaded: true,
			preloader: false,
		})
	);
});

export default authReducer;
