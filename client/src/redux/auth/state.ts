import User from "../../typing/dto/user/user.dto";

export default interface AuthState {
	user: User | null;
	userLoaded: boolean;
	preloader: boolean;
}

export const initialState: AuthState = {
	user: null,
	userLoaded: false,
	preloader: false,
};
