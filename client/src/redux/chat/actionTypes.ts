enum ChatActions {
	LoadMessages = "chat:messages:load",
	LoadMessagesInfo = "chat:messages:info:load",
	CreateMessage = "chat:messages:create",
	UpdateMessage = "chat:messages:update",
	StartUpdatingMessage = "chat:messages:update:start",
	CancelUpdatingMessage = "chat:messages:update:cancel",
	LikeMessage = "chat:messages:like",
	DeleteMessage = "chat:messages:delete",
}

export default ChatActions;
