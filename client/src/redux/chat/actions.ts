import { createAction, createAsyncThunk } from "@reduxjs/toolkit";

import {
	getMessageInfo,
	getMessages,
	createMessage as create,
	updateMessage as update,
	likeMessage as like,
	deleteMessage as remove,
} from "../../services/message.service";

import Message from "../../typing/dto/message/message.dto";
import RootState from "../state";
import ChatActions from "./actionTypes";
import ChatState from "./state";

const replaceUpdatedMessage = (
	messages: Message[],
	newMessage: Message
): Partial<ChatState> => {
	const newMessages = [...messages];
	const index = newMessages.findIndex((m) => m._id === newMessage._id);

	newMessages[index] = newMessage;

	return {
		messages: newMessages,
	};
};

export const loadMessages = createAsyncThunk<Partial<ChatState>>(
	ChatActions.LoadMessages,
	async () => ({
		messages: await getMessages(),
	})
);

export const loadMessagesInfo = createAsyncThunk<Partial<ChatState>>(
	ChatActions.LoadMessagesInfo,
	async () => ({
		info: await getMessageInfo(),
	})
);

export const createMessage = createAsyncThunk<Partial<ChatState>, string>(
	ChatActions.CreateMessage,
	async (text, { getState }) => {
		const {
			chat: { messages, info },
		} = getState() as RootState;

		const message = await create(text);

		return {
			messages: [...messages, message],
			...(info
				? {
						info: {
							...info,
							lastMessageDate: message.createdAt,
							messagesCount: info.messagesCount + 1,
						},
				  }
				: {}),
		};
	}
);

export const updateMessage = createAsyncThunk<
	Partial<ChatState>,
	{ id: string; text: string }
>(ChatActions.UpdateMessage, async ({ id, text }, { getState }) => {
	const {
		chat: { messages },
	} = getState() as RootState;

	const message = await update(id, text);
	return replaceUpdatedMessage(messages, message);
});

export const startUpdatingMessage = createAction<Message>(
	ChatActions.StartUpdatingMessage
);

export const cancelUpdatingMessage = createAction<void>(
	ChatActions.CancelUpdatingMessage
);

export const likeMessage = createAsyncThunk<Partial<ChatState>, string>(
	ChatActions.LikeMessage,
	async (id, { getState }) => {
		const {
			chat: { messages },
		} = getState() as RootState;

		const message = await like(id);
		return replaceUpdatedMessage(messages, message);
	}
);

export const deleteMessage = createAsyncThunk<Partial<ChatState>, string>(
	ChatActions.DeleteMessage,
	async (id, { getState }) => {
		const {
			chat: { messages, info },
		} = getState() as RootState;

		await remove(id);
		const newMessages = [...messages];
		const index = newMessages.findIndex((m) => m._id === id);

		newMessages.splice(index, 1);

		return {
			messages: newMessages,
			...(info
				? {
						info: {
							...info,
							lastMessageDate:
								newMessages[newMessages.length - 1].createdAt,
							messagesCount: info.messagesCount - 1,
						},
				  }
				: {}),
		};
	}
);
