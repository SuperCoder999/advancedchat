import { createReducer, isAnyOf } from "@reduxjs/toolkit";
import { createUser, deleteUser } from "../users/actions";

import {
	cancelUpdatingMessage,
	createMessage,
	deleteMessage,
	likeMessage,
	loadMessages,
	loadMessagesInfo,
	startUpdatingMessage,
	updateMessage,
} from "./actions";

import ChatState, { initialState } from "./state";

const chatReducer = createReducer<ChatState>(initialState, (builder) => {
	builder.addCase(loadMessages.pending, (state) => ({
		...state,
		preloader: true,
		messagesLoaded: false,
	}));

	builder.addCase(loadMessages.fulfilled, (state, { payload }) => ({
		...state,
		...payload,
		preloader: false,
		messagesLoaded: true,
	}));

	builder.addCase(loadMessages.rejected, (state) => ({
		...state,
		preloader: false,
		messagesLoaded: true,
	}));

	builder.addCase(createUser.fulfilled, (state) => ({
		...state,
		info: state.info
			? {
					...state.info,
					usersCount: (state.info?.usersCount ?? 0) + 1,
			  }
			: null,
	}));

	builder.addCase(updateMessage.fulfilled, (state, { payload }) => ({
		...state,
		...payload,
		preloader: false,
		editModal: false,
		updatingMessage: null,
	}));

	builder.addCase(startUpdatingMessage, (state, { payload }) => ({
		...state,
		editModal: true,
		updatingMessage: payload,
	}));

	builder.addCase(cancelUpdatingMessage, (state) => ({
		...state,
		editModal: false,
		updatingMessage: null,
	}));

	builder.addCase(deleteUser.fulfilled, (state) => ({
		...state,
		info: state.info
			? {
					...state.info,
					usersCount: (state.info?.usersCount ?? 0) - 1,
			  }
			: null,
	}));

	builder.addMatcher(
		isAnyOf(
			loadMessagesInfo.pending,
			createMessage.pending,
			updateMessage.pending,
			likeMessage.pending,
			deleteMessage.pending
		),
		(state) => ({
			...state,
			preloader: true,
		})
	);

	builder.addMatcher(
		isAnyOf(
			loadMessagesInfo.fulfilled,
			createMessage.fulfilled,
			likeMessage.fulfilled,
			deleteMessage.fulfilled
		),
		(state, { payload }) => ({
			...state,
			...payload,
			preloader: false,
		})
	);

	builder.addMatcher(
		isAnyOf(
			loadMessagesInfo.rejected,
			createMessage.rejected,
			updateMessage.rejected,
			likeMessage.rejected,
			deleteMessage.rejected
		),
		(state) => ({
			...state,
			preloader: false,
		})
	);
});

export default chatReducer;
