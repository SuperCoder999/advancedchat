import Message from "../../typing/dto/message/message.dto";
import MessagesInfo from "../../typing/dto/message/messages-info.dto";

export default interface ChatState {
	info: MessagesInfo | null;
	messages: Message[];
	messagesLoaded: boolean;
	editModal: boolean;
	updatingMessage: Message | null;
	preloader: boolean;
}

export const initialState: ChatState = {
	info: null,
	messages: [],
	messagesLoaded: false,
	editModal: false,
	updatingMessage: null,
	preloader: false,
};
