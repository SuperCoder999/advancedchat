import { combineReducers } from "@reduxjs/toolkit";
import { ReducerWithInitialState } from "@reduxjs/toolkit/dist/createReducer";
import authReducer from "./auth/reducer";
import chatReducer from "./chat/reducer";
import usersReducer from "./users/reducer";

const rootReducer: Record<string, ReducerWithInitialState<any>> = {
	auth: authReducer,
	chat: chatReducer,
	users: usersReducer,
};

export default rootReducer;
export const combinedReducer = combineReducers(rootReducer);
