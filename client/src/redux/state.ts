import AuthState from "./auth/state";
import ChatState from "./chat/state";
import UsersState from "./users/state";

export default interface RootState {
	auth: AuthState;
	chat: ChatState;
	users: UsersState;
}
