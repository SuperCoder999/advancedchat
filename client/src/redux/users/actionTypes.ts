enum UsersActions {
	LoadUsers = "users:load",
	CreateUser = "users:create",
	UpdateUser = "users:update",
	DeleteUser = "users:delete",
}

export default UsersActions;
