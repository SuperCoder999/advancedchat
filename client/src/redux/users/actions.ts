import { createAsyncThunk } from "@reduxjs/toolkit";

import {
	getUsers,
	createUser as create,
	updateUser as update,
	deleteUser as remove,
} from "../../services/user.service";

import UserModify from "../../typing/dto/user/user-modify.dto";
import RootState from "../state";
import UsersActions from "./actionTypes";
import UsersState from "./state";

export const loadUsers = createAsyncThunk<Partial<UsersState>>(
	UsersActions.LoadUsers,
	async () => ({
		users: await getUsers(),
	})
);

export const createUser = createAsyncThunk<Partial<UsersState>, UserModify>(
	UsersActions.CreateUser,
	async (data, { getState }) => {
		const {
			users: { users },
		} = getState() as RootState;

		if (!users) {
			return {};
		}

		const user = await create(data);

		return {
			users: [user, ...users],
		};
	}
);

export const updateUser = createAsyncThunk<
	Partial<UsersState>,
	{ id: string; data: Partial<UserModify> }
>(UsersActions.UpdateUser, async ({ id, data }, { getState }) => {
	const {
		users: { users },
	} = getState() as RootState;

	if (!users) {
		return {};
	}

	const user = await update(id, data);
	const newUsers = [...users];
	const index = newUsers.findIndex((m) => m._id === user._id);

	newUsers[index] = user;

	return {
		users: newUsers,
	};
});

export const deleteUser = createAsyncThunk<Partial<UsersState>, string>(
	UsersActions.DeleteUser,
	async (id, { getState }) => {
		const {
			users: { users },
		} = getState() as RootState;

		if (!users) {
			return {};
		}

		await remove(id);
		const newUsers = [...users];
		const index = newUsers.findIndex((m) => m._id === id);

		newUsers.splice(index, 1);

		return {
			users: newUsers,
		};
	}
);
