import { createReducer, isAnyOf } from "@reduxjs/toolkit";
import { createUser, deleteUser, loadUsers, updateUser } from "./actions";
import UsersState, { initialState } from "./state";

const usersReducer = createReducer<UsersState>(initialState, (builder) => {
	builder.addCase(loadUsers.pending, (state) => ({
		...state,
		preloader: true,
		usersLoaded: false,
	}));

	builder.addCase(loadUsers.fulfilled, (state, { payload }) => ({
		...state,
		...payload,
		preloader: false,
		usersLoaded: true,
	}));

	builder.addCase(loadUsers.rejected, (state) => ({
		...state,
		preloader: false,
		usersLoaded: true,
	}));

	builder.addMatcher(
		isAnyOf(createUser.pending, updateUser.pending, deleteUser.pending),
		(state) => ({
			...state,
			preloader: true,
		})
	);

	builder.addMatcher(
		isAnyOf(
			createUser.fulfilled,
			updateUser.fulfilled,
			deleteUser.fulfilled
		),
		(state, { payload }) => ({
			...state,
			...payload,
			preloader: false,
		})
	);

	builder.addMatcher(
		isAnyOf(createUser.rejected, updateUser.rejected, deleteUser.rejected),
		(state, { payload }) => ({
			...state,
			preloader: false,
		})
	);
});

export default usersReducer;
