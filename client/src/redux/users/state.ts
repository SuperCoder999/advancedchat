import User from "../../typing/dto/user/user.dto";

export default interface UsersState {
	users: User[];
	usersLoaded: boolean;
	preloader: boolean;
}

export const initialState: UsersState = {
	users: [],
	usersLoaded: false,
	preloader: false,
};
