import LocalStorageKeys from "../enums/local-storage-keys.enum";
import callWebApi from "../helpers/call-web-api.helper";
import forceRedirectLogin from "../helpers/redirect.helper";
import AuthorizedUser from "../typing/dto/user/authorized-user.dto";
import UserLogin from "../typing/dto/user/user-login.dto";
import User from "../typing/dto/user/user.dto";
import { HttpMethod } from "../typing/http";

export async function authenticate(body: UserLogin): Promise<User> {
	const response = await callWebApi({
		endpoint: "/auth/login",
		method: HttpMethod.Post,
		body,
	});

	const authorizedUser = (await response.json()) as AuthorizedUser;
	localStorage.setItem(LocalStorageKeys.UserToken, authorizedUser.token);

	return authorizedUser.user;
}

export function logoutUser(): void {
	forceRedirectLogin();
}

export async function getProfile(): Promise<User | null> {
	const token = localStorage.getItem(LocalStorageKeys.UserToken);

	if (!token) {
		return null;
	}

	try {
		const response = await callWebApi({
			endpoint: "/auth/profile",
			method: HttpMethod.Get,
		});

		return (await response.json()) as User;
	} catch {
		return null;
	}
}
