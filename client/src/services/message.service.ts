import callWebApi from "../helpers/call-web-api.helper";
import { HttpMethod } from "../typing/http";
import Message from "../typing/dto/message/message.dto";
import MessagesInfo from "../typing/dto/message/messages-info.dto";

export async function getMessages(): Promise<Message[]> {
	const response = await callWebApi({
		endpoint: "/message",
		method: HttpMethod.Get,
	});

	return (await response.json()) as Message[];
}

export async function getMessageInfo(): Promise<MessagesInfo> {
	const response = await callWebApi({
		endpoint: "/message/info",
		method: HttpMethod.Get,
	});

	return (await response.json()) as MessagesInfo;
}

export async function createMessage(text: string): Promise<Message> {
	const response = await callWebApi({
		endpoint: "/message",
		method: HttpMethod.Post,
		body: { text },
	});

	return (await response.json()) as Message;
}

export async function updateMessage(
	id: string,
	text: string
): Promise<Message> {
	const response = await callWebApi({
		endpoint: `/message/${id}`,
		method: HttpMethod.Patch,
		body: { text },
	});

	return (await response.json()) as Message;
}

export async function likeMessage(id: string): Promise<Message> {
	const response = await callWebApi({
		endpoint: `/message/${id}/like`,
		method: HttpMethod.Patch,
	});

	return (await response.json()) as Message;
}

export async function deleteMessage(id: string): Promise<void> {
	await callWebApi({
		endpoint: `/message/${id}`,
		method: HttpMethod.Delete,
	});
}
