import callWebApi from "../helpers/call-web-api.helper";
import { HttpMethod } from "../typing/http";
import User from "../typing/dto/user/user.dto";
import UserModify from "../typing/dto/user/user-modify.dto";

export async function getUsers(): Promise<User[]> {
	const response = await callWebApi({
		endpoint: "/user",
		method: HttpMethod.Get,
	});

	return (await response.json()) as User[];
}

export async function createUser(body: UserModify): Promise<User> {
	const response = await callWebApi({
		endpoint: "/user",
		method: HttpMethod.Post,
		body,
	});

	return (await response.json()) as User;
}

export async function updateUser(
	id: string,
	body: Partial<UserModify>
): Promise<User> {
	const response = await callWebApi({
		endpoint: `/user/${id}`,
		method: HttpMethod.Patch,
		body,
	});

	return (await response.json()) as User;
}

export async function deleteUser(id: string): Promise<void> {
	await callWebApi({
		endpoint: `/user/${id}`,
		method: HttpMethod.Delete,
	});
}
