import User from "../user/user.dto";

export default interface Message {
	_id: string;
	user: User;
	text: string;
	likeCount?: number;
	likers?: string[];
	createdAt: Date;
	updatedAt: Date;
	editedAt?: Date;
}
