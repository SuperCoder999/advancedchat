export default interface MessagesInfo {
	usersCount: number;
	messagesCount: number;
	lastMessageDate: Date | null;
}
