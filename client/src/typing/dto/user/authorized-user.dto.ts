import User from "./user.dto";

export default interface AuthorizedUser {
	user: User;
	token: string;
}
