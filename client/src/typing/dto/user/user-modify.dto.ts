export default interface UserModify {
	name: string;
	email: string;
	password?: string;
	avatar?: string;
	isAdmin: boolean;
}
