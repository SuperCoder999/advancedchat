export default interface User {
	_id: string;
	name: string;
	email: string;
	avatar?: string;
	isAdmin: boolean;
	createdAt: Date;
	updatedAt: Date;
}
