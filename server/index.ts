import * as dotenv from "dotenv";
dotenv.config(); // Must be called before env.config import

import server, { startup, shutdown } from "./src/server";
import env from "./src/constants/env.constants";
import handleSignals from "./src/utils/helpers/signal.helper";
import Signal from "./src/enums/signals.enum";

const main = async () => {
	await startup();

	const instance = server.listen(
		env.application.port,
		env.application.host,
		() => console.log(`Server is running on port ${env.application.port}`)
	);

	handleSignals([Signal.SigInt, Signal.SigTerm], () => {
		console.log("Shutting down...");

		instance.close();
		shutdown();
	});
};

main();
