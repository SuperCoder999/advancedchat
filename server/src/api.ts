import { Router } from "express";
import { authRouter, messageRouter, userRouter } from "./routers";

const apiRouter = Router();

apiRouter.use("/auth", authRouter);
apiRouter.use("/user", userRouter);
apiRouter.use("/message", messageRouter);

export default apiRouter;
