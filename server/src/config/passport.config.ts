import passport from "passport";
import { Types } from "mongoose";
import { Strategy as BearerStrategy } from "passport-http-bearer";
import { AccessDeniedError } from "../exceptions";
import { userService } from "../services";
import { decryptJwt } from "../utils/helpers/jwt.helper";

const configurePassport = () => {
	passport.use(
		"jwt",
		new BearerStrategy((token, next) => {
			try {
				const { id } = decryptJwt(token);

				userService
					.retrieve({ _id: new Types.ObjectId(id) })
					.then((user) =>
						user ? next(null, user) : next(new AccessDeniedError())
					)
					.catch(() => next(new AccessDeniedError()));
			} catch (error) {
				next(new AccessDeniedError());
			}
		})
	);
};

export default configurePassport;
