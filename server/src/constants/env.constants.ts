const { HOST, PORT, MONGO_URI, JWT_SECRET, PASSWORD_SECURITY_LEVEL } =
	process.env;

const env = {
	application: {
		port: PORT ? parseInt(PORT) : 3001,
		host: HOST ?? "0.0.0.0",
	},
	database: {
		uri: MONGO_URI ?? "",
	},
	jwt: {
		secret: JWT_SECRET ?? "",
	},
	password: {
		securityLevel: PASSWORD_SECURITY_LEVEL
			? parseInt(PASSWORD_SECURITY_LEVEL)
			: 4,
	},
};

export default env;
