import { userService } from "../services";
import Controller from "../typing/controller";
import withErrorHandling from "../utils/helpers/handle-http-errors.helper";

const applyAuthController: Controller = (router) => {
	router.get("/profile", (req, res) => res.json(req.user));

	router.post(
		"/login",
		withErrorHandling(async (req, res) => {
			const result = await userService.login(req.body);
			res.json(result);
		})
	);

	return router;
};

export default applyAuthController;
