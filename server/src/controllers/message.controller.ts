import { Types } from "mongoose";
import HttpStatus from "../enums/http-status.enum";
import { messageService } from "../services";
import Controller from "../typing/controller";
import { User } from "../typing/models";
import withErrorHandling from "../utils/helpers/handle-http-errors.helper";

const applyMessageController: Controller = (router) => {
	router.get(
		"/",
		withErrorHandling(async (_, res) => {
			const result = await messageService.list();
			res.json(result);
		})
	);

	router.get(
		"/info",
		withErrorHandling(async (_, res) => {
			const result = await messageService.getInfo();
			res.json(result);
		})
	);

	router.post(
		"/",
		withErrorHandling(async (req, res) => {
			const { _id } = await messageService.create({
				...req.body,
				user: (req.user as User)._id,
			});

			const result = await messageService.retrieve({ _id });
			res.status(HttpStatus.Created).json(result);
		})
	);

	router.patch(
		"/:id",
		withErrorHandling(async (req, res) => {
			const _id = new Types.ObjectId(req.params.id);

			await messageService.update(
				{ ...req.body, editedAt: new Date() },
				{ _id }
			);

			const result = await messageService.retrieve({ _id });
			res.send(result);
		})
	);

	router.patch(
		"/:id/like",
		withErrorHandling(async (req, res) => {
			const _id = new Types.ObjectId(req.params.id);
			const result = await messageService.like(req.user as User, _id);

			res.send(result);
		})
	);

	router.delete(
		"/:id",
		withErrorHandling(async (req, res) => {
			const _id = new Types.ObjectId(req.params.id);
			await messageService.delete({ _id });

			res.status(HttpStatus.NoContent).json();
		})
	);

	return router;
};

export default applyMessageController;
