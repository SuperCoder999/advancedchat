import { Types } from "mongoose";
import HttpStatus from "../enums/http-status.enum";
import { onlyAdmin } from "../middleware";
import { userService } from "../services";
import Controller from "../typing/controller";
import withErrorHandling from "../utils/helpers/handle-http-errors.helper";

const applyUserController: Controller = (router) => {
	router.get(
		"/",
		onlyAdmin,
		withErrorHandling(async (_, res) => {
			const result = await userService.list();
			res.json(result);
		})
	);

	router.post(
		"/",
		onlyAdmin,
		withErrorHandling(async (req, res) => {
			const { _id } = await userService.create(req.body);
			const result = await userService.retrieve({ _id });

			res.status(HttpStatus.Created).json(result);
		})
	);

	router.patch(
		"/:id",
		onlyAdmin,
		withErrorHandling(async (req, res) => {
			const _id = new Types.ObjectId(req.params.id);

			await userService.update(req.body, { _id });
			const result = await userService.retrieve({ _id });

			res.send(result);
		})
	);

	router.delete(
		"/:id",
		onlyAdmin,
		withErrorHandling(async (req, res) => {
			const _id = new Types.ObjectId(req.params.id);
			await userService.delete({ _id });

			res.status(HttpStatus.NoContent).json();
		})
	);

	return router;
};

export default applyUserController;
