import mongoose from "mongoose";
import env from "../constants/env.constants";

export default class DBConnector {
	public static connect() {
		return mongoose.connect(env.database.uri);
	}

	public static disconnect() {
		return mongoose.disconnect();
	}
}
