enum HttpStatus {
	Created = 201,
	NoContent = 204,
	Unauthorized = 401,
	Forbidden = 403,
	InternalServerError = 500,
}

export default HttpStatus;
