enum Signal {
	SigInt = "SIGINT",
	SigTerm = "SIGTERM",
}

export default Signal;
