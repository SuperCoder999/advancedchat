import HttpStatus from "../enums/http-status.enum";
import { ErrorDto } from "../typing/dto";

export default class AbstractError extends Error {
	public isHttpError = true;
	public statusCode: HttpStatus;

	public constructor(statusCode: HttpStatus, message: string) {
		super(message);
		this.statusCode = statusCode;
	}

	public getResponse = (): ErrorDto => {
		return { message: this.message };
	};

	public static fromError(error: Error): AbstractError {
		if (error.hasOwnProperty("isHttpError")) {
			return error as AbstractError;
		}

		return new AbstractError(HttpStatus.InternalServerError, error.message);
	}
}
