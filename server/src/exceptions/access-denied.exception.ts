import HttpStatus from "../enums/http-status.enum";
import AbstractError from "./abstract.exception";

export default class AccesDeniedError extends AbstractError {
	public constructor() {
		super(HttpStatus.Forbidden, "Access denied");
	}
}
