import HttpStatus from "../enums/http-status.enum";
import AbstractError from "./abstract.exception";

export default class InvalidCredentialsError extends AbstractError {
	public constructor() {
		super(HttpStatus.Unauthorized, "Invalid email or password");
	}
}
