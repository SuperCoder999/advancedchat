import { RequestHandler } from "express";
import passport from "passport";
import authenticationWhiteList from "../constants/auth-whitelist.constants";
import { AccessDeniedError } from "../exceptions";

const authenticate: RequestHandler = (req, res, next) => {
	if (
		authenticationWhiteList.includes(req.path) ||
		req.method === "OPTIONS"
	) {
		return next();
	}

	const middleware = passport.authenticate(
		"jwt",
		{ session: false },
		(err, user, _) => {
			if (err || !user) {
				return next(new AccessDeniedError());
			}

			req.user = user;
			next();
		}
	);
	middleware(req, res, next);
};

export default authenticate;
