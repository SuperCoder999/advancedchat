import { RequestHandler } from "express";

const cors: RequestHandler = (_, res, next) => {
	res.setHeader("Access-Control-Allow-Origin", "*");
	res.setHeader("Access-Control-Allow-Headers", "*");
	res.setHeader("Access-Control-Allow-Methods", "*");

	next();
};

export default cors;
