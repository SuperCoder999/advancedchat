import { ErrorRequestHandler } from "express";
import { AbstractError } from "../exceptions";

const handleErrors: ErrorRequestHandler = (err, _req, res, _next) => {
	const httpError = AbstractError.fromError(err);
	res.status(httpError.statusCode).send(httpError.getResponse());
};

export default handleErrors;
