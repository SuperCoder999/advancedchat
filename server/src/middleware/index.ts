export { default as authenticate } from "./authenticate.middleware";
export { default as onlyAdmin } from "./only-admin.middleware";
export { default as handleErrors } from "./error.middleware";
export { default as cors } from "./cors.middleware";
