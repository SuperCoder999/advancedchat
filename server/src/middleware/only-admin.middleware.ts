import { RequestHandler } from "express";
import { AccessDeniedError } from "../exceptions";
import { User } from "../typing/models";

const onlyAdmin: RequestHandler = (req, _, next) => {
	if (!(req.user as User | undefined)?.isAdmin) {
		return next(new AccessDeniedError());
	}

	next();
};

export default onlyAdmin;
