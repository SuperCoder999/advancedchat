import { Schema, model } from "mongoose";

const schema = new Schema(
	{
		text: {
			type: String,
			required: true,
		},
		likeCount: {
			type: Number,
			required: true,
			default: 0,
		},
		likers: {
			type: [{ type: Schema.Types.ObjectId, ref: "User" }],
			default: [],
			required: true,
			select: false,
		},
		user: {
			type: Schema.Types.ObjectId,
			ref: "User",
			required: true,
		},
		editedAt: {
			type: Date,
			default: undefined,
		},
	},
	{
		collection: "messages",
		timestamps: true,
	}
);

const MessageModel = model("Message", schema);
export default MessageModel;
