import { Schema, model } from "mongoose";
import { hashPasswordSync } from "../utils/helpers/hash.helper";

const schema = new Schema(
	{
		name: {
			type: String,
			required: true,
		},
		email: {
			type: String,
			required: true,
			unique: true,
		},
		password: {
			type: String,
			required: true,
			select: false,
		},
		isAdmin: {
			type: Boolean,
			default: false,
		},
		avatar: {
			type: String,
			default: undefined,
		},
	},
	{
		collection: "users",
		timestamps: true,
	}
);

schema.pre("save", function (next) {
	if (this.isModified("password") || this.isNew) {
		this.password = hashPasswordSync(this.password);
	}

	next();
});

schema.pre("updateOne", function (next) {
	if (this.isModified("password") || this.isNew) {
		this.password = hashPasswordSync(this.password);
	}

	next();
});

const UserModel = model("User", schema);
export default UserModel;
