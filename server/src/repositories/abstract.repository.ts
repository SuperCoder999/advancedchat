import { AnyObject, Model } from "mongoose";
import { AbstractInterface } from "../typing/models";

export default class AbstractRepository<
	TModel extends Model<any>,
	TInterface extends AbstractInterface
> {
	public Model: TModel;

	public constructor(Model: TModel) {
		this.Model = Model;
	}

	public list(filter?: Partial<TInterface>): Promise<TInterface[]> {
		return this.Model.find(filter ?? {})
			.lean()
			.exec();
	}

	public count(filter?: Partial<TInterface>): Promise<number> {
		return this.Model.count(filter ?? {})
			.lean()
			.exec();
	}

	public retrieve(
		filter?: Partial<TInterface>
	): Promise<TInterface | undefined> {
		return this.Model.findOne(filter ?? {})
			.lean()
			.exec();
	}

	public create(data: AnyObject): Promise<TInterface> {
		return this.Model.create(data);
	}

	public async update(
		data: Partial<AnyObject>,
		filter?: Partial<TInterface>
	): Promise<void> {
		await this.Model.updateOne(filter ?? {}, data)
			.lean()
			.exec();
	}

	public async delete(filter?: Partial<TInterface>): Promise<void> {
		await this.Model.deleteMany(filter ?? {});
	}
}
