import UserRepository from "./user.repository";
import MessageRepository from "./message.repository";

const userRepository = new UserRepository();
const messageRepository = new MessageRepository();

export { userRepository, messageRepository };
export { default as AbstractRepository } from "./abstract.repository";
