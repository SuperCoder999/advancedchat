import { Types } from "mongoose";
import { MessageModel } from "../models";
import { Message } from "../typing/models";
import AbstractRepository from "./abstract.repository";

export default class MessageRepository extends AbstractRepository<
	typeof MessageModel,
	Message
> {
	public constructor() {
		super(MessageModel);
	}

	public override list(filter?: Partial<Message>): Promise<Message[]> {
		return this.Model.find(filter ?? {})
			.sort({ createdAt: "asc" })
			.populate("user")
			.lean()
			.exec() as Promise<Message[]>;
	}

	public override retrieve(filter?: Partial<Message>): Promise<Message> {
		return this.Model.findOne(filter ?? {})
			.populate("user")
			.lean()
			.exec() as Promise<Message>;
	}

	public async getLastDate(): Promise<Date | undefined> {
		const messageAsArray = await this.Model.find({})
			.sort({ createdAt: "desc" })
			.limit(1)
			.lean()
			.exec();

		if (messageAsArray.length === 0) {
			return;
		}

		return messageAsArray[0].createdAt;
	}

	public getLikesInfo(_id: Types.ObjectId): Promise<Message> {
		return this.Model.findOne({ _id })
			.select("likers likeCount")
			.lean()
			.exec();
	}
}
