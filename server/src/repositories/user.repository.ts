import { UserModel } from "../models";
import { User } from "../typing/models";
import AbstractRepository from "./abstract.repository";

export default class UserRepository extends AbstractRepository<
	typeof UserModel,
	User
> {
	public constructor() {
		super(UserModel);
	}

	public getPassword(filter?: Partial<User>) {
		return this.Model.findOne(filter).select("password").lean().exec();
	}
}
