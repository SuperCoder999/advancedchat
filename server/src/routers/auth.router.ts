import { Router } from "express";
import { applyAuthController } from "../controllers";

const authRouter = Router();
applyAuthController(authRouter);

export default authRouter;
