import { Router } from "express";
import { applyMessageController } from "../controllers";

const messageRouter = Router();
applyMessageController(messageRouter);

export default messageRouter;
