import { Router } from "express";
import { applyUserController } from "../controllers";

const userRouter = Router();
applyUserController(userRouter);

export default userRouter;
