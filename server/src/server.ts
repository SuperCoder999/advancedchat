import express from "express";
import passport from "passport";
import apiRouter from "./api";
import configurePassport from "./config/passport.config";
import DBConnector from "./db/db-connector";
import { authenticate, cors, handleErrors } from "./middleware";

const server = express();
configurePassport();

server.use(express.json());
server.use(express.urlencoded({ extended: true }));

server.use(cors);
server.use(passport.initialize());
server.use(authenticate);

server.use("/api", apiRouter);

server.use(handleErrors);

const startup = async () => {
	await DBConnector.connect();
};

const shutdown = async () => {
	await DBConnector.disconnect();
};

export default server;
export { startup, shutdown };
