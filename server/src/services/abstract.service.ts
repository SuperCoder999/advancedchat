import { Model } from "mongoose";
import { AbstractInterface } from "../typing/models";
import AbstractRepository from "../repositories/abstract.repository";

export default class AbstractService<
	TModel extends Model<any>,
	TInterface extends AbstractInterface,
	TDto,
	TRepository extends AbstractRepository<
		TModel,
		TInterface
	> = AbstractRepository<TModel, TInterface>
> {
	public repository: TRepository;

	public constructor(repository: TRepository) {
		this.repository = repository;
	}

	public list(filter?: Partial<TInterface>): Promise<TInterface[]> {
		return this.repository.list(filter);
	}

	public retrieve(
		filter?: Partial<TInterface>
	): Promise<TInterface | undefined> {
		return this.repository.retrieve(filter);
	}

	public create(data: TDto): Promise<TInterface> {
		return this.repository.create(data);
	}

	public async update(
		data: Partial<TDto>,
		filter?: Partial<TInterface>
	): Promise<void> {
		await this.repository.update(data, filter);
	}

	public async delete(filter?: Partial<TInterface>) {
		await this.repository.delete(filter);
	}
}
