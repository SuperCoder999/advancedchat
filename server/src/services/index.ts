import UserService from "./user.service";
import MessageService from "./message.service";

const userService = new UserService();
const messageService = new MessageService();

export { userService, messageService };
export { default as AbstractService } from "./abstract.service";
