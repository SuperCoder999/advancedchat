import { MessageModel } from "../models";
import { Message, User } from "../typing/models";
import { MessageDto, MessageInfoDto } from "../typing/dto";
import { messageRepository, userRepository } from "../repositories";
import AbstractService from "./abstract.service";
import MessageRepository from "../repositories/message.repository";
import { Types } from "mongoose";

export default class MessageService extends AbstractService<
	typeof MessageModel,
	Message,
	MessageDto,
	MessageRepository
> {
	public constructor() {
		super(messageRepository);
	}

	public async getInfo(): Promise<MessageInfoDto> {
		const usersCount = await userRepository.count();
		const messagesCount = await this.repository.count();
		const lastMessageDate = await this.repository.getLastDate();

		return { usersCount, messagesCount, lastMessageDate };
	}

	public async like(user: User, id: Types.ObjectId): Promise<Message> {
		let diff = 1;
		const message = await messageRepository.getLikesInfo(id);
		const newLikers = [...message.likers];

		const likerIndex = message.likers.findIndex((liker) =>
			liker.equals(user._id)
		);

		if (likerIndex !== -1) {
			diff = -1;
		}

		if (diff === 1) {
			newLikers.push(user._id);
		} else {
			newLikers.splice(likerIndex, 1);
		}

		await this.update(
			{ likeCount: message.likeCount + diff, likers: newLikers },
			{ _id: id }
		);

		return (await this.retrieve({ _id: id })) as Message;
	}
}
