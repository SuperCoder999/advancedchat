import { UserModel } from "../models";
import { User } from "../typing/models";
import { UserAuthenticatedDto, UserDto, UserLoginDto } from "../typing/dto";
import { userRepository } from "../repositories";
import AbstractService from "./abstract.service";
import InvalidCredentialsError from "../exceptions/invalid-credentials.exception";
import { checkPassword } from "../utils/helpers/hash.helper";
import { encryptJwt } from "../utils/helpers/jwt.helper";
import UserRepository from "../repositories/user.repository";
import { userToUserSafeDto } from "../utils/mappers/user.mapper";

export default class UserService extends AbstractService<
	typeof UserModel,
	User,
	UserDto,
	UserRepository
> {
	public constructor() {
		super(userRepository);
	}

	public async login(dto: UserLoginDto): Promise<UserAuthenticatedDto> {
		const user = await this.repository.getPassword({ email: dto.email });

		if (!user) {
			throw new InvalidCredentialsError();
		}

		const validPassword = await checkPassword(dto.password, user.password);

		if (!validPassword) {
			throw new InvalidCredentialsError();
		}

		const safeUser = await this.repository.retrieve({ _id: user._id });

		return {
			user: userToUserSafeDto(safeUser as User),
			token: encryptJwt({ id: user._id }),
		};
	}
}
