export default interface MessageInfoDto {
	messagesCount: number;
	usersCount: number;
	lastMessageDate?: Date;
}
