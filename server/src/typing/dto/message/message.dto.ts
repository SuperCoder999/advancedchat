import { Types } from "mongoose";

export default interface MessageDto {
	_id: Types.ObjectId;
	__v: number;
	text: string;
	likeCount: number;
	likers: Types.ObjectId[];
	user: Types.ObjectId;
	editedAt?: Date;
}
