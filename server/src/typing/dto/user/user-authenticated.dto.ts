import UserSafeDto from "./user-safe.dto";

export default interface UserAuthenticatedDto {
	user: UserSafeDto;
	token: string;
}
