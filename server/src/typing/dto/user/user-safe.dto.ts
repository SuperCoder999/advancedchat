import { Types } from "mongoose";

export default interface UserSafeDto {
	_id: Types.ObjectId;
	__v: number;
	name: string;
	email: string;
	avatar?: string;
	isAdmin: boolean;
	createdAt: Date;
	updatedAt: Date;
}
