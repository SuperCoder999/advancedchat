import { Types } from "mongoose";

export default interface UserDto {
	_id: Types.ObjectId;
	__v: number;
	name: string;
	email: string;
	password: string;
	isAdmin: boolean;
	avatar?: string;
}
