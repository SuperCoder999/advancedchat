import { Types } from "mongoose";

export default interface AbstractInterface {
	_id: Types.ObjectId;
	__v: number;
	createdAt: Date;
	updatedAt: Date;
}
