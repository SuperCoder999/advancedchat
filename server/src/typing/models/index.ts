export type { default as User } from "./user.interface";
export type { default as Message } from "./message.interface";
export type { default as AbstractInterface } from "./abstract.interface";
