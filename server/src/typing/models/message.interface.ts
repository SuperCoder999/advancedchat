import { Types } from "mongoose";
import User from "./user.interface";
import AbstractInterface from "./abstract.interface";

export default interface Message extends AbstractInterface {
	text: string;
	likeCount: number;
	likers: Types.ObjectId[];
	user: User;
}
