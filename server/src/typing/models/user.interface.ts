import AbstractInterface from "./abstract.interface";

export default interface User extends AbstractInterface {
	name: string;
	email: string;
	password: string;
	isAdmin: boolean;
	avatar?: string;
}
