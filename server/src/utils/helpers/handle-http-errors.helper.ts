import { RequestHandler, Request, Response, NextFunction } from "express";

export default function withErrorHandling(
	handler: (req: Request, res: Response, next: NextFunction) => Promise<any>
): RequestHandler {
	return (req, res, next) => handler(req, res, next).catch(next);
}
