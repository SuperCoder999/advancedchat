import * as bcrypt from "bcrypt";
import env from "../../constants/env.constants";

export function hashPassword(password: string): Promise<string> {
	return bcrypt.hash(password, env.password.securityLevel);
}

export function hashPasswordSync(password: string): string {
	return bcrypt.hashSync(password, env.password.securityLevel);
}

export function checkPassword(
	password: string,
	hash: string
): Promise<boolean> {
	return bcrypt.compare(password, hash);
}
