import * as jwt from "jsonwebtoken";
import env from "../../constants/env.constants";

export function encryptJwt(data: Record<string, any>): string {
	return jwt.sign(data, env.jwt.secret);
}

export function decryptJwt(token: string): Record<string, any> {
	return jwt.verify(token, env.jwt.secret);
}
