import Signal from "../../enums/signals.enum";

export default function handleSignals(signals: Signal[], handler: () => any) {
	signals.forEach((signal) => process.on(signal, handler));
}
