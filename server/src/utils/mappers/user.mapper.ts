import UserSafeDto from "../../typing/dto/user/user-safe.dto";
import { User } from "../../typing/models";

export function userToUserSafeDto(user: User): UserSafeDto {
	return {
		_id: user._id,
		__v: user.__v,
		name: user.name,
		email: user.email,
		isAdmin: user.isAdmin,
		avatar: user.avatar,
		createdAt: user.createdAt,
		updatedAt: user.updatedAt,
	};
}
